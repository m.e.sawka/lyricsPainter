import sys, json
import os
#import time
from bson import objectid
from wordcloud import WordCloud, STOPWORDS, ImageColorGenerator
from PIL import Image
from scipy.misc import imread, imresize
import numpy as np
from datetime import datetime
import pymongo
import re
import random

def stopwords_list():
    stopwords = {'outro', 'cuty', 'hook', 'guitar', 'solo', 'chorus', 'verse', 'Guitar', 'Solo', 'Chorus', 'Verse', 'Intro', 'Outro', 'x2', 'ref', 'x3', '3x', '2x', 'zwrotka', 'refren', 'x2', 'x4', '4x', 'Ref', '2x', 'a', 'aby', 'ach', 'acz', 'aczkolwiek', 'aj', 'albo', 'ale', 'ależ', 'ani', 'aż', 'bardziej', 'bardzo', 'bo', 'bowiem', 'by', 'byli', 'bynajmniej', 'być', 'był', 'była', 'było', 'były', 'będzie', 'będą', 'cali', 'cała', 'cały', 'ci', 'cię', 'ciebie', 'co', 'cokolwiek', 'coś', 'czasami', 'czasem', 'czemu', 'czy', 'czyli', 'daleko', 'dla', 'dlaczego', 'dlatego', 'do', 'dobrze', 'dokąd', 'dość', 'dużo', 'dwa', 'dwaj', 'dwie', 'dwoje', 'dziś', 'dzisiaj', 'gdy', 'gdyby', 'gdyż', 'gdzie', 'gdziekolwiek', 'gdzieś', 'i', 'ich', 'ile', 'im', 'inna', 'inne', 'inny', 'innych', 'iż', 'ja', 'ją', 'jak', 'jakaś', 'jakby', 'jaki', 'jakichś', 'jakie', 'jakiś', 'jakiż', 'jakkolwiek', 'jako', 'jakoś', 'je', 'jeden', 'jedna', 'jedno', 'jednak', 'jednakże', 'jego', 'jej', 'jemu', 'jest', 'jestem', 'jeszcze', 'jeśli', 'jeżeli', 'już', 'ją', 'każdy', 'kiedy', 'kilka', 'kimś', 'kto', 'ktokolwiek', 'ktoś', 'która', 'które', 'którego', 'której', 'który', 'których', 'którym', 'którzy', 'ku', 'lat', 'lecz', 'lub', 'ma', 'mają', 'mało', 'mam', 'mi', 'mimo', 'między', 'mną', 'mnie', 'mogą', 'moi', 'moim', 'moja', 'moje', 'może', 'możliwe', 'można', 'mój', 'mu', 'musi', 'my', 'na', 'nad', 'nam', 'nami', 'nas', 'nasi', 'nasz', 'nasza', 'nasze', 'naszego', 'naszych', 'natomiast', 'natychmiast', 'nawet', 'nią', 'nic', 'nich', 'nie', 'niech', 'niego', 'niej', 'niemu', 'nigdy', 'nim', 'nimi', 'niż', 'no', 'o', 'obok', 'od', 'około', 'on', 'ona', 'one', 'oni', 'ono', 'oraz', 'oto', 'owszem', 'pan', 'pana', 'pani', 'po', 'pod', 'podczas', 'pomimo', 'ponad', 'ponieważ', 'powinien', 'powinna', 'powinni', 'powinno', 'poza', 'prawie', 'przecież', 'przed', 'przede', 'przedtem', 'przez', 'przy', 'roku', 'również', 'sama', 'są', 'się', 'skąd', 'sobie', 'sobą', 'sposób', 'swoje', 'ta', 'tak', 'taka', 'taki', 'takie', 'także', 'tam', 'te', 'tego', 'tej', 'temu', 'ten', 'teraz', 'też', 'to', 'tobą', 'tobie', 'toteż', 'trzeba', 'tu', 'tutaj', 'twoi', 'twoim', 'twoja', 'twoje', 'twym', 'twój', 'ty', 'tych', 'tylko', 'tym', 'u', 'w', 'wam', 'wami', 'was', 'wasz', 'wasza', 'wasze', 'we', 'według', 'wiele', 'wielu', 'więc', 'więcej', 'wszyscy', 'wszystkich', 'wszystkie', 'wszystkim', 'wszystko', 'wtedy', 'wy', 'właśnie', 'z', 'za', 'zapewne', 'zawsze', 'ze', 'zł', 'znowu', 'znów', 'został', 'żaden', 'żadna', 'żadne', 'żadnych', 'że', 'żeby'}
    stopwords_en = {"a","able","about","across","after","all","almost","also","am","among","an","and","any","are","as","at","be","because","been","but","by","can","cannot","could","dear","did","do","does","either","else","ever","every","for","from","get","got","had","has","have","he","her","hers","him","his","how","however","i","i'm", "it's", "i'll","don't", "if","in","into","is","it","its","just","least","let","like","likely","may","me","might","most","must","my","neither","no","nor","not","of","off","often","on","only","or","other","our","own","rather","said","say","says","she","should","since","so","some","than","that","the","their","them","then","there","these","they","this","tis","to","too","twas","us","wants","was","we","were","what","when","where","which","while","who","whom","why","will","with","would","yet","you","your"}
    stopwords = stopwords.union(stopwords_en)
    return stopwords


def createMaskFromText(txt, fontPath):
    from PIL import Image
    from PIL import ImageFont
    from PIL import ImageDraw
    img = Image.new("RGB", (600, 600), 0)
    # img = Image.new("RGBA", (600, 600), 255)
    draw = ImageDraw.Draw(img)
    W = img.size[0]
    H = img.size[1]
    fontsize = 1
    font = ImageFont.load_default()
    font = ImageFont.truetype(fontPath, fontsize)

    lines = txt.upper().split(' ')
    longestLine = max(lines, key=len)

    textWidth=0
    textHeight=0

    offset = 10
    while textWidth  < W - offset and textHeight < H - offset:
        fontsize += 1
        font = ImageFont.truetype(fontPath, fontsize)
        textWidth=font.getsize(longestLine)[0]
        textHeight=font.getsize(longestLine)[1]*len(lines)

    w, h = draw.textsize(txt, font)
    topLineStart = (H-textHeight)/2 - offset * 2

    for line in lines:
        print(line)
        print(offset)
        draw.text((10, topLineStart + offset), line, font=font, fill="#FFF")
        offset += font.getsize(line)[1]

    return img

def custom_color_func(colorHsl, addRandom):
        hslString = 'hsl(' + str(int(colorHsl['h'])) + ', ' + str(int(colorHsl['s'])) + '%, ' + str(abs(int
        (colorHsl['l']) + addRandom * random.randint(0,10) - 5)) + '%)'

        return hslString

def custom_bg_color_func(colorHsl):
        hslString = 'hsl(' + str(int(colorHsl['h'])) + ', ' + str(int(colorHsl['s'])) + '%, ' + str(abs(int(colorHsl['l'])))+ '%)'

        return hslString


def main():
    artist_name = sys.argv[1]
    artistId = int(sys.argv[2])
    working_dir = sys.argv[3]
    userOpts = json.loads(sys.argv[4])
    print(userOpts)
    wordColor = userOpts['wordColor']
    backgroundColor = userOpts['backgroundColor']
    maskFileName = userOpts['maskFileName']
    tmpMaskFilePath = userOpts['tmpMaskFilePath']
    fonts = {"Plaster": "/Plaster-Regular.ttf", "Elite": "/SpecialElite.ttf", "Rubik": "/RubikMonoOne-Regular.ttf", "Grace": "/CoveredByYourGrace.ttf", "Anton":"/Anton-Regular.ttf", "Roboto": "/Roboto-Regular.ttf"}
    artistNameFont = os.path.dirname(__file__) + str(fonts[userOpts['artistNameFont']])
    wc_font_path = os.path.dirname(__file__) + str(fonts[userOpts['wordcloudFont']])
    print(wc_font_path)
    print(maskFileName)
    print(tmpMaskFilePath)
    lyrics = ''

    client = pymongo.MongoClient(host='127.0.0.1', port=3001)
    db = client['meteor']
    songsCollection = db.songs
    songs = songsCollection.find({"primary_artist.id": artistId})
    for song in songs:
        try:
            if len(song['lyrics'])>20:
                lyrics = lyrics + song['lyrics']
        except KeyError:
            print ('No lyrics for song "%s"' % song['title'])
        except ValueError:
            print ('PARSE ERROR "%s"' % song['title'])
    print('lyrics length '+ str(len(lyrics)))

    lyrics = re.sub('\[.*?\]', '', lyrics)

    stopwords = stopwords_list()
    #for word in artist_name.split(' '):
    #    stopwords.add(word)
    img = Image.new("RGBA", (600, 600))
    mask = np.array(img)

    path = os.path.dirname(__file__)
    print(tmpMaskFilePath)
    if tmpMaskFilePath is not None:
        #iterations = 7
        #print('------file exists ' + str(os.path.isfile(tmpMaskFilePath)))
        #while not os.path.isfile(tmpMaskFilePath) and iterations > 0:
        #    print('Waiting for file ' + str(iterations * 3) + 'more seconds')
        #    iterations -=1
        #    time.sleep(3)
        imgArr = imread(tmpMaskFilePath, False, 'RGB')
        mask = imresize(imgArr, (600, 600))
        os.remove(tmpMaskFilePath)
        print('mask size' + str(mask.size))
        print('mask.shape' + str(mask.shape))
    else:
        mask = np.array(createMaskFromText(artist_name, artistNameFont))

    def word_color_function(word, font_size, position, orientation, random_state=None, **kwargs):
        return custom_color_func(wordColor, 1)

    backgroundColorHsl = custom_bg_color_func(backgroundColor)

    word_cloud = WordCloud(background_color=backgroundColorHsl, max_words=1000, mask = mask, stopwords=stopwords, color_func=word_color_function, font_path = wc_font_path)
    word_tuples = WordCloud.process_text(word_cloud, lyrics)
    sorted_tuples = sorted(word_tuples, key=lambda tup: tup[1])

    word_cloud.generate(lyrics)

    generated_image_id = objectid.ObjectId()
    file_name = str(generated_image_id) + ".png"
    artistDirName = re.sub('[^\x00-\x7F]', '_', artist_name.strip().replace(" ", "_").replace(".", "").replace("/", "").lower())
    directory = working_dir + "/.images/" + artistDirName + "/"
    full_file_name = directory + file_name
    if not os.path.exists(directory):
        os.makedirs(directory)

    word_cloud.to_file(full_file_name)
    images = db.imageIds
    object_to_save = {
        'artist': artist_name,
        '_id': generated_image_id,
        'date': datetime.now().isoformat(),
        'custom': tmpMaskFilePath
    }
    images.insert_one(object_to_save)
    print("SAVED:\n" + str(object_to_save))
    print(generated_image_id)
    return(generated_image_id)

    return 'main'


if __name__ == '__main__':
    main()
