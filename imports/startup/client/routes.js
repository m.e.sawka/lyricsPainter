import {FlowRouter} from 'meteor/kadira:flow-router';
import {BlazeLayout} from 'meteor/kadira:blaze-layout';
// Import needed templates
import '../../ui/layouts/body/body.js';
import '../../ui/pages/createImage/createImage.js';
import '../../ui/pages/popular/popular.js';
import '../../ui/pages/singleArtist/singleArtist.js';
import '../../ui/components/singleImage/singleImage.js';
import '../../ui/components/suggestions/suggestions.js';
import '../../ui/pages/gallery/gallery.js';
import '../../ui/pages/gallery/kingsOfRap/kingsOfRap.js';
import '../../ui/pages/gallery/monstersOfRock/monstersOfRock.js';
import '../../ui/pages/about/about.js';
import '../../ui/pages/stats/stats.js';
import '../../ui/pages/not-found/not-found.js';
import {getColorObject, invertColor} from './../../support/colorFunctions'
var randomColor = require('randomcolor'); // import the script

// Set up all routes in the app
FlowRouter.route('/', {
  name: 'App.home',
  action() {
    addMeta();
    Session.keys = {};
    BlazeLayout.render('App_body', {main: 'about'});
  },
});

FlowRouter.route('/popular', {
  name: 'popular',
  action() {
    BlazeLayout.render('App_body', {main: 'popular'});
  },
});

FlowRouter.route('/gallery', {
  name: 'gallery',
  action(params, queryParams) {
    addMeta();
    BlazeLayout.render('App_body', {main: 'gallery'});
  },
});

FlowRouter.route('/gallery/kingsOfRap', {
  name: 'kingsOfRap',
  action(params, queryParams) {
    addMeta();
    BlazeLayout.render('App_body', {main: 'kingsOfRap'});
  },
});

FlowRouter.route('/gallery/monstersOfRock', {
  name: 'monstersOfRock',
  action(params, queryParams) {
    addMeta();
    BlazeLayout.render('App_body', {main: 'monstersOfRock'});
  },
});

FlowRouter.route('/stats', {
  name: 'stats',
  action() {
    addMeta();
    BlazeLayout.render('App_body', {main: 'stats'});
  },
});

FlowRouter.route('/create', {
  name: 'createImage',
  action(params, queryParams) {
    addMeta();
    BlazeLayout.render('App_body', {main: 'createImage'});
  },
  triggersEnter: [() => {

    const wordColor = getColorObject(randomColor());
    const backgroundColor = getColorObject(invertColor(wordColor.hex));
    Session.keys = {};
    Session.set('userOpts', {
      wordColor,
      backgroundColor,
      maskFileName: null,
      tmpMaskFilePath: null,
      artistNameFont: "Roboto",
      wordcloudFont: "Roboto",
    })
  }],
  // triggersExit: [() => Session.keys = {}]
});

FlowRouter.route('/suggestions', {
  name: 'suggestions',
  action(params, queryParams) {
    addMeta();
    BlazeLayout.render('App_body', {main: 'suggestions', params});
  },
});

FlowRouter.route('/about', {
  name: 'about',
  action(params, queryParams) {
    addMeta();
    BlazeLayout.render('App_body', {main: 'about'});
  },
});

FlowRouter.route('/:artistName', {
  name: 'singleArtist',
  action(params, queryParams) {
    addMeta(params);
    BlazeLayout.render('App_body', {main: 'singleArtist'});
  },
});

FlowRouter.route('/single/:artistName/:file', {
  name: 'singleImage',
  action(params, queryParams) {
    addMeta(params);
    BlazeLayout.reset();
    BlazeLayout.render('App_body', {main: 'singleImage'});
  }
});

FlowRouter.notFound = {
  action() {
    BlazeLayout.render('App_body', {main: 'App_notFound'});
  },
};


function addMeta(params = {}, image = 'https://lyricspainter.sawka.pro/examples/wu-tang_clan.png') {
  DocHead.removeDocHeadAddedTags();
  $('meta[property^="og:"]').remove();
  $('meta[property^="twitter:"]').remove();
  $('meta[itemprop]').remove();
  let imageUrl = image;
  if (FlowRouter.current().params.artistName && FlowRouter.current().params.file) {
    imageUrl = `https://lyricspainter.sawka.pro/images/${FlowRouter.current().params.artistName}/${FlowRouter.current().params.file}`;
  }
  const metaTags = [
    {
      property: "og:url",
      content: "https://lyricspainter.sawka.pro"
    }, {
      property: "og:type",
      content: "website"
    }, {
      name: "description",
      content: "LYRICSPAINTER - wordclouds with your favorite artist's lyrics"
    }, {
      property: "og:title",
      content: "LyricsPainter"
    }, {
      property: "og:description",
      content: "Wordclouds with your favorite artists' lyrics",
    }, {
      property: "og:image",
      content: imageUrl
    }, {
      property: "twitter:image",
      content: imageUrl
    }, {
      property: "twitter:title",
      content: 'LyricsPainter'
    }, {
      property: "twitter:description",
      content: 'LyricsPainter'
    }, {
      property: "twitter:card",
      content: 'summary_large_image'
    }, {
      property: "data-pin-url",
      content: imageUrl
    }, {
      property: "data-pin-media",
      content: imageUrl
    }
  ];

  metaTags.forEach((tag)=>{
    const selector = `meta[property^="${tag.property}"]`;
    $(selector).remove();
    DocHead.addMeta({
      property: tag.property,
      content: tag.content
    });
  })

}