import './body.html';
import {Meteor} from 'meteor/meteor';

  Template.App_body.onRendered(()=>{
    if (Meteor.isClient) {
      // function fb(d, s, id) {
      //   var js, fjs = d.getElementsByTagName(s)[0];
      //   if (d.getElementById(id)) return;
      //   js = d.createElement(s); js.id = id;
      //   js.src = "//connect.facebook.net/pl_PL/sdk.js#xfbml=1&version=v2.9&appId=865485733593064  ";
      //   fjs.parentNode.insertBefore(js, fjs);
      // };
     function pin (d){
        var f = d.getElementsByTagName('SCRIPT')[0], p = d.createElement('SCRIPT');
        p.type = 'text/javascript';
        p.async = true;
        p.src = '//assets.pinterest.com/js/pinit.js';
        f.parentNode.insertBefore(p, f);
      }

      const doc = window.document;
      pin(doc)
    }
  });


Template.App_body.helpers({
  shareData: {
    title: "LYRICSPAINTER",
    name: '',
    description: "Wordclouds with your favorite artist's lyrics",
    image: 'https://lyricspainter.sawka.pro/examples/wu-tang_clan.png',
  }
})