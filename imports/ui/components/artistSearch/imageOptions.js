import {Meteor} from 'meteor/meteor';
import './imageOptions.html';
import './canvasMask.html';
import './canvasMask.js';


Template.imageOptions.rendered = function() {
  const opts = Session.get("userOpts");
  $('#wordColor .colorpicker').colorpicker({
    color: opts.wordColor.hex
  }).on('changeColor',
    function(ev) {
      const id = ev.target.parentElement.getAttribute('id');
      const userOpts = Session.get('userOpts');
      const hsla = ev.color.toHSL();
      userOpts[id] = {
        h: Math.round(hsla.h*360),
        s: Math.round(hsla.s*100),
        l: Math.round(hsla.l*100),
        hex: ev.color.toHex()
      }
      Session.set('userOpts', userOpts);
    });
  $('#backgroundColor .colorpicker').colorpicker({
    color: opts.backgroundColor.hex
  }).on('changeColor',
    function(ev) {
      const id = ev.target.parentElement.getAttribute('id');
      const userOpts = Session.get('userOpts');
      const hsla = ev.color.toHSL();
      userOpts[id] = {
        h: Math.round(hsla.h*360),
        s: Math.round(hsla.s*100),
        l: Math.round(hsla.l*100),
        hex: ev.color.toHex()
      };
      Session.set('userOpts', userOpts);
    });
}

Template.imageOptions.events({
  'click .imageOptionsButton'(event) {
    event.preventDefault();
    event.target.blur();
    const imageOptionsDetails = window.document.getElementById('imageOptionsDetails');
    const detailsHidden = window.getComputedStyle(imageOptionsDetails).getPropertyValue('display') === 'none';
    if (detailsHidden){
      imageOptionsDetails.style.display = 'block';
    } else {
      imageOptionsDetails.style.display = 'none';
    }
  },
  'click .imageOption button'(event) {
    event.preventDefault();
    event.target.blur();
    const siblingElement = $(event.target).next()[0];

    const detailsHidden = siblingElement.style.display === 'none' || siblingElement.style.display ==="";
    if (detailsHidden){
      siblingElement.style.display = 'block';
    } else {
      siblingElement.style.display = 'none';
    }
  },
  'click input[type="radio"]' (event) {
   const attributeName = event.target.getAttribute('name');
   event.target.blur();
   const value = event.target.value;
   const userOpts = Session.get('userOpts') || {};
   userOpts[attributeName] = value;
    Session.set('userOpts', userOpts);
  },
  'change .colorpicker .input-group-addon i' (event) {
   const attributeName = event.target.getAttribute('name');
   const value = event.target.value;
   const userOpts = Session.get('userOpts') || {};
   userOpts[attributeName] = value;
    Session.set('userOpts', userOpts);
  },
  "change .file-upload-input": function(event, template){
    var file = event.currentTarget.files[0];
    var reader = new FileReader();
    reader.onload = function(fileLoadEvent) {
      Meteor.call('file-upload', file.name, reader.result);
    };
    reader.readAsBinaryString(file);
    Session.set('userOpts', {...Session.get('userOpts'), maskFileName: file.name });
  }
});

Template.fontPicker.events({
  'click input[type="radio"]' (event) {
   event.target.blur();
   const attributeName = event.target.parentElement.parentElement.id;
   const value = event.target.value;
   const userOpts = Session.get('userOpts') || {};
   userOpts[attributeName] = value;
    Session.set('userOpts', userOpts);
  },
});
