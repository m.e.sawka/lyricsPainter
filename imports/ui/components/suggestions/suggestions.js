import './suggestions.html'
import { getArtistCallback } from "../../../support/processNewArtist.js"


Template.suggestions.helpers({
  artistNames: function (){
    return Session.get('suggestions')
  },
  inputName: function (){
    return Session.get('inputName')
  },
})

Template.suggestions.events({
  'click button'(event){
    const artistName = event.target.getAttribute('data-value');
    FlowRouter.go('/create');
    Meteor.call('getArtist', artistName, getArtistCallback);
  },
})