import './singleImage.html';
import '/imports/ui/components/galleryRow/galleryRow.html';
import {ImageIds} from '/imports/api/genius/imageIds.js';
import { normalizeStringToURLPath, normalizeString }  from '/imports/support/normalizeName';

Template.singleImage.onCreated( () => {
  const artistName = FlowRouter.current().params.artistName.replace(/%20/gi, " ");
  Meteor.subscribe('imageIdsFor', artistName, 5);
});

Template.singleImage.helpers({
    shareData() {
      return {
        title: "LYRICSPAINTER - wordclouds with your favorite artist's lyrics",
        name: FlowRouter.current().params.artistName,
        image: `https://lyricspainter.sawka.pro/images/${FlowRouter.current().params.artistName}/${FlowRouter.current().params.file}`,
      }
    },
    image() {
      const params = FlowRouter.current().params;
      return `/images/${normalizeStringToURLPath(params.artistName)}/${params.file}`;
    },
    artist() {
      return {
        name: FlowRouter.current().params.artistName.replace(/%20/gi, " ").toUpperCase()
      };
    },
    otherImages() {
      const artistName = FlowRouter.current().params.artistName.replace(/%20/gi, " ");
      let mappedImages
      if (artistName) {
        const images = ImageIds.find({"artist": artistName}, {limit: 3, skip: 1, sort: {date: -1}}).fetch();
        mappedImages = images.map((element) => {
          element.filePath = `/images/${normalizeStringToURLPath(element.artist)}/${element._id.toHexString()}.png`;
          element.shareData = {
            title: "LYRICSPAINTER",
            name: element.artist,
            description: "Wordclouds with your favorite artist's lyrics",
            image: element.filePath,
          };
          element.normalizedArtistName = normalizeString(element.artist);
          element.fileName = element.filePath.substr(element.filePath.lastIndexOf("/")+1,50);
          return element;
        })
      }
      return mappedImages
    }
});