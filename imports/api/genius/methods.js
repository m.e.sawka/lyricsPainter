// Methods related to genius api
import {Meteor} from 'meteor/meteor';
import {Artists} from './artists.js';
import {ArtistsPending} from './artists.js';
import {Songs} from './songs.js';
import _ from 'underscore';

var cheerio = require('cheerio');
var accessToken = process.env.GENIUS_ACCESS_TOKEN;
var syncGet = Meteor.wrapAsync(HTTP.get);
var PythonShell = require('python-shell');
const fs = require('fs');
var syncPythonRun = Meteor.wrapAsync(PythonShell.run);
import { normalizeString } from '/imports/support/normalizeName';

Meteor.methods({
      'file-upload': function (fileName, fileData) {
        console.log("received file " + fileName );
        const filePath = `${process.env.PWD}/.images/tmp/${fileName}`;
        fs.writeFileSync(filePath, new Buffer(fileData, 'binary'));
        // fs.writeFile(filePath, new Buffer(fileData, 'binary', ()=>{console.log(`Saved file ${fileName}`)}));
      },
  'getArtist'(inputName) {
    inputName = inputName.trim();
    const normalizedInput = normalizeString(inputName);
    const artist = Artists.findOne({$or: [{artistName: inputName}, {normalizedName: normalizedInput}, {inputName}]});
    console.log('__________________artist', artist);
    if (artist) {
      const songsToUpdate = Songs.find({$and: [{'primary_artist.id': artist.artistId}, {$or: [{updated: {$exists :  false}}, {lyrics: {$exists :  false}}]}]}).count();
      Artists.update({artistName: inputName}, {$set: { songsToUpdate }});
      console.log('updating empty lyrics count', songsToUpdate);
    }
    const updatedArtist = Artists.findOne({$or: [{artistName: inputName}, {normalizedName: normalizedInput}, {inputName}]});
    return {updatedArtist, inputName};
  },
  'getNewArtist'(inputName) {
    const newArtistData = checkForNewArtistId(inputName);
    console.log('batman: newArtistData', newArtistData);
    return newArtistData;
  },
  'addEmailToPendingList'(artistName, email) {
    return ArtistsPending.insert({artistName, email});
  },
    'getLyricsForSong'(songDoc) {
        songDoc = songDoc ? songDoc : {
          "_id" : "YbPPuPnWarwj3LXmJ",
          "api_path" : "/songs/3231",
          "path" : "/Jay-z-03-bonnie-and-clyde-lyrics",
          "title" : "'03 Bonnie & Clyde by Jay Z (Ft. Beyoncé)",
          "primary_artist" : {
            "api_path" : "/artists/2",
            "header_image_url" : "https://images.genius.com/57f67753a0fe8ce5ff4a2ab903fbe850.500x301x1.png",
            "id" : 2,
            "image_url" : "https://images.genius.com/9a9a5e5de20f096449de14797ecf3b33.397x397x1.png",
            "is_meme_verified" : true,
            "is_verified" : true,
            "name" : "Jay Z",
            "url" : "https://genius.com/artists/Jay-z",
            "iq" : 175
          },
          "lyrics" : "\n\n[Verse 1: Jay Z]\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n[Verse 2: Jay Z]\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n[Verse 3: Beyoncé]\n(Talk to 'em, B!) \n\n\n\n\n\n\n"
        };
        // console.log('Getting lyrics from ', path);
      getSongLyrics(songDoc);
    },
    'getLyricsForAllSongs'(artist) {
        getLyricsForAllSongs(artist);
        return artist;
    },
    'getArtistSongs'(artist) {
        let page = '';
        let songs = [];

        do {
            result = getSongs(artist.artistId, page);
            songs = songs.concat(result.songs);
            page = result.nextPage;
        } while (page)

        var songsAsPrimaryArtist = songs.filter((song) => {
            const isPrimaryArtist = song.primary_artist.id === artist.artistId;
            const isSong = !song.title.includes('Album Art') && !song.title.includes('Tracklist') && !song.title.includes('Cover Art');
            return isPrimaryArtist && isSong;
        });

        console.log('Finished fetching songs metadata');

        const songPaths = songsAsPrimaryArtist.map((song) => {
            return song.api_path
        });
        const songsInDb = Songs.find({"api_path": {$in: songPaths}}).fetch();

        const songsToAdd = songsAsPrimaryArtist.filter((song) => {
            const songIsAlreadyInDb = songsInDb.find((songInDb) => {
                return songInDb.api_path == song.api_path;
            });
            return !songIsAlreadyInDb;
        });

        console.log('Songs in db ', songsInDb.length);
        console.log('Going to add ', songsToAdd.length, 'songs');
        let start = new Date();
        songsToAdd.forEach((song) => {
            Songs.insert(song);
            console.log('Insert time:', new Date().valueOf() - start.valueOf(), song.title);
            start = new Date();
        });
        return artist;
    },
    spawnImage: function (artistName, artistId, userOpts) {
        console.log('SPAWN START', new Date());
        const startTime = new Date();
        const pythonScript = process.env.IMG_GEN_SCRIPT;
        const pythonScriptPath = process.env.SCRIPTS_PATH;
        const pythonExecPath = process.env.PYTHON_3_PATH;

        if (userOpts.maskFileName) {
            const tmpMaskFilePath =`${process.env.PWD}/.images/tmp/${userOpts.maskFileName}`;
            userOpts.tmpMaskFilePath = tmpMaskFilePath;
        }
        console.log('userOpts', JSON.stringify(userOpts), new Date());

        var options = {
            mode: 'text',
            pythonPath: pythonExecPath,
            scriptPath: pythonScriptPath,
            args: [artistName, artistId, process.env.PWD, JSON.stringify(userOpts)]
        };

        const pythonConsoleOutput = syncPythonRun(pythonScript, options);
        const createdImageId = pythonConsoleOutput[pythonConsoleOutput.length - 1];
        pythonConsoleOutput.forEach((line) => {
            console.log(line)
        });
        console.log(`   Script execution time: ${Math.round((new Date() - startTime) / 10) / 100}s`, new Date());
        return createdImageId;
    },
});

function checkForNewArtistId(inputName) {
    let names = [];
    let artistId;
    let status;
    let name;
    let normalizedName;
    let i = 0;
    let artist = { inputName };
    let artistName;
    const normalizedInput = normalizeString(inputName);
    const start = new Date();
    data = syncGet('https://api.genius.com/search?access_token=' + accessToken + '&q=' + encodeURIComponent(inputName), {});
    console.log('New artist request time', new Date().valueOf() - start.valueOf());

    do {
        const primary_artist = data.data.response.hits[i].result.primary_artist;
        if (primary_artist) {
            name = primary_artist.name;
            artistName = primary_artist.name;
            normalizedName = normalizeString(primary_artist.name);
            artistId = primary_artist.id;
            names.push(name);
        }
        i++;
    } while (normalizedName != normalizedInput && i < data.data.response.hits.length);

    if (normalizedName == normalizedInput) {
        status = 'SUCCESS';
        artist = {
          name,
          artistName,
          normalizedName,
          inputName,
          artistId,
          createdAt: new Date(),
          songsToUpdate: 999
        };
        Artists.insert(artist);
    } else {
        status = 'FAIL';
    }
    names = _.uniq(names);
    return {names, status, artist, inputName}
}

function getSongs(artistId, page) {
    const uriToEncode = page ? "per_page=50&page=" + page : "per_page=50&page=1";
    const start = new Date();
    request = syncGet('https://api.genius.com/artists/' + artistId + '/songs?access_token=' + accessToken + '&' + uriToEncode);
    console.log('Songlist request time', new Date().valueOf() - start.valueOf());

    const nextPage = request.data.response.next_page;
    const songs = request.data.response.songs.map(getSongData);

    return {songs, nextPage};
}

function getSongData(song) {
    return {
        api_path: song.api_path,
        path: song.path,
        title: song.full_title,
        primary_artist: song.primary_artist
    }
}

function getSongLyrics(songDoc, index = 1, lastIndex = 1) {
    const start = new Date();
    HTTP.get('https://genius.com' + songDoc.path, (error, data) => {
      if (error) {
        console.log('Error: with page https://genius.com' + songDoc.path, error);
        Songs.remove({_id: songDoc._id});
      } else if (data.content) {
        let $ = cheerio.load(data.content, {decodeEntities: false});
        let lyricsHTML = $('lyrics').html() || $('.lyrics').html();
        let lyrics = $('lyrics').text() || $('.lyrics').text();

        if (!lyricsHTML
          || lyrics.length < 30
          || lyrics.includes('<b>Tracklist</b>')
          || lyrics.includes('<b>Cover Art:</b>')
          || lyrics.includes('Unfortunately, we are not licensed to display the full lyrics for this song at the momen')
        ) {
          console.log('Setting lyrics to EMPTY for ', 'https://genius.com' + songDoc.path);
          lyrics = 'EMPTY';
        }
        let songToSave = songDoc;
        songToSave.lyrics = lyrics;
        songToSave.updated = new Date();
        console.log('Updated:', index, '/', lastIndex, songDoc.title, 'Request time', new Date().valueOf() - start.valueOf());
        Songs.update({_id: songDoc._id}, songToSave);
      } else {
        console.log('No response content for song', songDoc.title);
        console.log('Data received', data);
      }
    })
}


function getLyricsForAllSongs({artist, forceUpdate = false}) {
    const artistId = artist.artistId;
    const songs = Songs.find({"primary_artist.id": artistId}).fetch();
    let songsToUpdate = songs.filter((song) => !song.lyrics || !song.updated);
    console.log('Lyrics will be updated for ', songsToUpdate.length, 'songs of', artist.artistName);
    songsToUpdate.forEach((song, index) => {
        getSongLyrics(song, index, songsToUpdate.length);
    });
    Meteor.setTimeout(()=>{
      const songsToUpdate = Songs.find({$and: [{'primary_artist.id': artist.artistId}, {$or: [{updated: {$exists :  false}}, {lyrics: {$exists :  false}}]}]}).count();
      Artists.update({artistName: artist.artistName}, {$set: { songsToUpdate, 'updated': new Date()}});
      console.log('UPDATING ARTIST', artist);
    },30000);
    return artist
}
