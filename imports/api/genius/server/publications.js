import { Meteor } from 'meteor/meteor';
import { ImageIds } from '../imageIds.js';
import { Songs } from '../songs.js';
import { Artists } from '../artists.js';

Meteor.publish('songs', function () {
  //TODO limit to one artist
  return Songs.find();
});

Meteor.publish('artists', function () {
  return Artists.find();
});

Meteor.publish('artists.single', function (artistId) {
  return Artists.find({artistId}, {
    sort: { date: -1 },
  });
});

Meteor.publish('imageIds', function () {
  return ImageIds.find();
});

Meteor.publish('imageIdsFor', function (artist, limit) {
  return ImageIds.find({artist}, {
    limit: limit,
    sort: { date: -1 },
  });
});

Meteor.publish('latestImages', function(limit) {
  return ImageIds.find({}, {
    limit: limit,
    sort: { date: -1 },
  });
});
