import './leaveEmail.html';

Template.leaveEmailAddress.events({
  'submit #leaveEmailAddress'(event) {
    event.preventDefault();
    const artistName = Session.get('currentArtist').name;
    const email = window.document.getElementById('email').value;
    Meteor.call('addEmailToPendingList', artistName, email, ()=>{
      window.document.getElementById('submitMessage').innerHTML = 'I will send your wordcloud for '+ Session.get('currentArtist').name + ' to ' + email + ' when its ready '
    })
  }
});
