def createMaskFromText(txt='NO SIEMA'):
    from PIL import Image
    from PIL import ImageFont
    from PIL import ImageDraw
    img = Image.new("RGB", (600, 600), 0)
    # img = Image.new("RGBA", (600, 600), 255)
    draw = ImageDraw.Draw(img)
    W = img.size[0]
    H = img.size[1]
    font = ImageFont.load_default()
    fontsize = 1

    lines = txt.upper().split(' ')
    longestLine = max(lines, key=len)

    textWidth=0
    textHeight=0

    offset = 10
    while textWidth  < W - offset and textHeight < H - offset:
        fontsize += 1
        #font = ImageFont.truetype(os.path.dirname(__file__)+"/Roboto-Regular.ttf", fontsize)
        font = ImageFont.truetype(os.path.dirname(__file__)+"/Anton-Regular.ttf", fontsize)
        textWidth=font.getsize(longestLine)[0]
        textHeight=font.getsize(longestLine)[1]*len(lines)


    w, h = draw.textsize(txt, font)
    topLineStart = (H-textHeight)/2 - offset * 2

    for line in lines:
        print(line)
        print(offset)
        draw.text((10, topLineStart + offset), line, font=font, fill="#FFF")
        offset += font.getsize(line)[1]

    img.save(os.path.dirname(__file__)+"/generatedTextFile.png")

    return img