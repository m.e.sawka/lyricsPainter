import Tabular from 'meteor/aldeed:tabular';
import './stats.html';
import {Artists} from '/imports/api/genius/artists.js';

Template.stats.onCreated(function () {
  Meteor.subscribe('artists');
});

Template.stats.helpers({
  selector() {
    return {stats: {$exists :  true}, "stats.wordcount": {$gte: 5000}}
  },
});
