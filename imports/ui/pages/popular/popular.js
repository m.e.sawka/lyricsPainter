import './popular.html';
import {Artists} from '/imports/api/genius/artists.js';

Template.popular.onCreated(function () {
    Meteor.subscribe('artists');
});

Template.popular.helpers({
    artists() {
        const artists = Artists.find({}, {limit:50,  sort: { images : -1 }}).fetch();
        return artists;
    },
});
