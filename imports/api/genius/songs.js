// Definition of the songs collection

import { Mongo } from 'meteor/mongo';

// Songs = new Mongo.Collection('songs');
export const Songs = new Mongo.Collection('songs');
