// Import modules used by both client and server through a single index entry point
// e.g. useraccounts configuration file.
import Tabular from 'meteor/aldeed:tabular';
import {Artists} from '/imports/api/genius/artists.js';

new Tabular.Table({
  name: "stats",
  collection: Artists,
  columns: [
    {data: "artistName", title: "Artist", render: (val)=>{
      return `<a class="artistName" href ="/${val}">${val}</a>`
    }},
    {data: "stats.avg_word_length", title: "Average word length",
      render: function (val, type, doc) {
        return Math.round(val*10)/10
      }
    },
    {data: "stats.avg_filtered_word_length", title: "Average filtered word length",
      render: function (val, type, doc) {
        return Math.round(val*10)/10
      }
    },
    {data: "stats.most_common", title: "Most common words",
      render: function (val, type, doc) {
        if(val && val.length && val.length > 0) {
          const most_common_words = val.map((tuple)=>{
            return tuple[0]
          });
          return most_common_words.slice(0,15).join('\n\n\t');
        }
      }
    },
    // {
    //   tmpl: Meteor.isClient && Template.bookCheckOutCell
    // }
  ]
});