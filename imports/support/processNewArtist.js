export function getArtistCallback(error, opts)  {
  const inputName = opts.inputName;
  const artistInDB = opts.updatedArtist;
  if (artistInDB) {
    //TODO second if should be fixed
    if (artistInDB.updated
      && (!artistInDB.songsToUpdate || artistInDB.songsToUpdate == 0)) {
      Session.set('currentArtist', null);
      Session.set('currentArtist', artistInDB);
      let artistName = artistInDB.artistName;
      console.log(artistName + ' in db', new Date());
      Session.set('generatedImageId', null);
      Session.set('imageGenStatus', ['Your image will be generated soon. Please be patient :)']);
      makeImage(error = null, artistInDB)
      Meteor.subscribe('imageIdsFor', artistInDB.artistName, 5);
    } else if (!artistInDB.updated || artistInDB.songsToUpdate > 0) {
      //TODO might be deleted?
      console.log(inputName, 'data not up to date. Updating, ', new Date().toLocaleString());
      Session.set('imageGenStatus', ['We are getting '+ inputName +' lyrics especially for you now.' , 'Feel free to' +
      ' leave your email and I will let you know when the image is ready, or try again in 5 minutes.']);
      Session.set('leaveEmail', true);
      Meteor.call('getArtistSongs', artistInDB, updateAllLyrics);
    }
  } else {
    console.log(inputName + ' NOT in db');
    console.log(inputName, 'not in db. Getting all data, ', new Date().toLocaleString());
    Meteor.call('getNewArtist', inputName, processNewArtist);
  }
}


export function processNewArtist(error, result) {
  const artist = result && result.artist ? result.artist : null;
  if (error) {
    console.log('Failed to get ID for:', result, '|| REASON:', error.reason);
    Session.set('imageGenStatus', [`There was some error... Cannot find data for this artist.`, `Please try again later or send a report`]);
  } else if (result && result.status == 'FAIL') {
    //TODO this is probably not working/needed
    Session.set('imageGenStatus', [`I can't find data for ${result.inputName}`, `Please try some of these`, ...result.names]);
    Session.set('suggestions', result.names);
    Session.set('inputName', result.inputName);
    FlowRouter.go('/suggestions');
  } else if (result && result.status == 'SUCCESS') {
    Session.set('currentArtist', artist);
    if (result.artist.updated) {
      //TODO this is probably not working/needed
      makeImage(error=null, artist);
    } else {
      Session.set('imageGenStatus', ['We don\'t have data for '+ result.inputName +' yet.', 'The data is being downloaded especially for you now.' , 'Feel free to leave your email and I will let you know when the image is ready, or try again in a few minutes.']);
      Session.set('leaveEmail', true);
      console.log('START GETTING ALL LYRICS', new Date());
      Meteor.call('getArtistSongs', artist, getAllLyricsAndOnlySave);
    }
  }
}

export function getAllLyricsAndOnlySave(error, artist) {
  if (error) {
    console.log('getAllLyricsAndOnlySave error', error);
  } else {
    Meteor.call('getLyricsForAllSongs', {artist});
  }
}

//TODO might be deleted?
export function updateAllLyrics(error, artist) {
  if (error) {
    console.log('getAllLyricsAndOnlySave error', error);
  } else {
    const forceUpdate = true;
    Meteor.call('getLyricsForAllSongs', {artist, forceUpdate});
  }
}

export function makeImage(error, artist) {
  const userOpts = Session.get('userOpts') || {};
  console.log('PAINTING!', new Date());
  const artistName = artist.inputName ? artist.inputName : artist.artistName;
  Meteor.call('spawnImage', artistName, artist.artistId, userOpts, (error, generatedImageId) => {
    if (error) {
      console.log('Image generation failed', error.reason);
    } else {
      console.log('Generated image Id is ', generatedImageId);
      Session.set('generatedImageId', generatedImageId);
      Session.set('imageGenStatus', null);
      console.log('IMAGE GENERATION END', new Date());
    }
  });
}

export function saveCanvasMask() {
  const canvas = window.document.getElementById('paint');
  let artistName = document.querySelector('#artistInput').value;
  var dataURL = canvas.toDataURL('image/png');
  var file = dataURLtoBlob(dataURL);
  var reader = new FileReader();
  const maskFileName = artistName + new Date().valueOf() + '.png';
  const tmpMaskFilePath =`${process.env.PWD}/.images/tmp/${maskFileName}`;
  reader.onload = function(fileLoadEvent) {
    Meteor.call('file-upload', maskFileName, fileLoadEvent.target.result);
  };
  reader.readAsBinaryString(file);
  Session.set('userOpts', {...Session.get('userOpts'), maskFileName, tmpMaskFilePath  });
}

export function dataURLtoBlob(dataurl) {
  var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
    bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
  while(n--){
    u8arr[n] = bstr.charCodeAt(n);
  }
  return new Blob([u8arr], {type:mime});
}
