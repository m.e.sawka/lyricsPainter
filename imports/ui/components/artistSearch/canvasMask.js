import {Meteor} from 'meteor/meteor';
import './canvasMask.html';
///TODO all the code here should be refactored

Template.canvasMask.rendered = function() {
  const canvas = document.getElementById('paint');
  if (window.innerWidth< canvas.width){
    const newCanvasHeight = Math.round(0.9*window.innerWidth);
    canvas.height = newCanvasHeight;
    canvas.width = newCanvasHeight;
  }
  if (window.innerHeight < canvas.height){
    const newCanvasHeight = Math.round(0.9*window.innerHeight);
    canvas.height = newCanvasHeight;
    canvas.width = newCanvasHeight;
  }
  const ctx = canvas.getContext('2d');
  let mouse = {x: 0, y: 0};
  let last_mouse = {x: 0, y: 0};

  document.body.addEventListener("touchstart", function (e) {
    var touch = e.touches[0];
    if (e.target == canvas) {
      e.preventDefault();
    }
  }, false);
  document.body.addEventListener("touchend", function (e) {
    var touch = e.touches[0];
    if (e.target == canvas) {
      e.preventDefault();
    }
    canvas.addEventListener('mousemove', onPaint, false);
  }, false);
  document.body.addEventListener("touchmove", function (e) {
    var touch = e.touches[0];
    canvas.addEventListener('mousemove', onPaint, false);
    if (e.target == canvas) {
      e.preventDefault();
    }
  }, false);



  canvas.addEventListener("touchstart", function (e) {
    mousePos = getTouchPos(canvas, e);
    var touch = e.touches[0];
    var mouseEvent = new MouseEvent("mousedown", {
      clientX: touch.clientX,
      clientY: touch.clientY
    });
    canvas.dispatchEvent(mouseEvent);
  }, false);

  canvas.addEventListener("touchend", function (e) {
    var mouseEvent = new MouseEvent("mouseup", {});
    canvas.dispatchEvent(mouseEvent);
  }, false);
  canvas.addEventListener("touchmove", function (e) {
    var touch = e.touches[0];

    var rect = canvas.getBoundingClientRect();
    last_mouse.x = mouse.x;
    last_mouse.y = mouse.y;
    mouse.x = touch.clientX - rect.left;
    mouse.y = touch.clientY- rect.top;
    var mouseEvent = new MouseEvent("mousemove", {
      clientX: mouse.x,
      clientY: mouse.y,
    });
    canvas.dispatchEvent(mouseEvent);
  }, false);


// Get the position of a touch relative to the canvas
  function getTouchPos(canvasDom, touchEvent) {
    var rect = canvasDom.getBoundingClientRect();
    return {
      x: touchEvent.touches[0].clientX - rect.left,
      y: touchEvent.touches[0].clientY - rect.top
    };
  }

  /* Mouse Capturing Work */
  canvas.addEventListener('mousemove', function(e) {
    const canvas = window.document.getElementById('paint');
    var rect = canvas.getBoundingClientRect();
    last_mouse.x = mouse.x;
    last_mouse.y = mouse.y;
    if (!is_touch_device()) {
      mouse.x = e.pageX - rect.left - window.scrollX;
      mouse.y = e.pageY - rect.top - window.scrollY;
    }else{
      mouse.x = mouse.x+1
    }
  }, false);

  var brushColor = 'white';

  ctx.lineWidth = 15;
  ctx.lineJoin = 'round';
  ctx.lineCap = 'round';
  ctx.strokeStyle = brushColor;

  canvas.addEventListener('mousedown', function(e) {
    canvas.addEventListener('mousemove', onPaint, false);
  }, false);

  canvas.addEventListener('mouseup', function() {
    canvas.removeEventListener('mousemove', onPaint, false);
  }, false);

  var onPaint = function() {
    if (Session.get('canvasImage') != true) {
      Session.set('canvasImage', true);
    }
    ctx.beginPath();
    ctx.moveTo(last_mouse.x, last_mouse.y);
    ctx.lineTo(mouse.x, mouse.y);
    ctx.closePath();
    ctx.stroke();
  };
}

function is_touch_device() {
  return (('ontouchstart' in window)
  || (navigator.MaxTouchPoints > 0)
  || (navigator.msMaxTouchPoints > 0));
}

Template.canvasMask.events({
  'click #clearCanvas': function(event) {
    event.preventDefault();
    event.target.blur();
    const canvas = window.document.getElementById('paint');
    const context = canvas.getContext('2d');
    context.clearRect(0, 0, canvas.width, canvas.height);
  }
})