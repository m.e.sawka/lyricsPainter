import './singleArtist.html';
import './../../components/gallery/gallery_template.html';
import { normalizeString, normalizeStringToURLPath } from '/imports/support/normalizeName';
import {ImageIds} from '/imports/api/genius/imageIds.js';

Template.singleArtist.onCreated(function () {
    const artist = FlowRouter.current().path.replace("/", "").replace(/%20/gi, " ");
    Session.set('limit', 3);
    this.autorun(function () {
        const limit = Session.get('limit');
        Meteor.subscribe('imageIdsFor', artist, limit);
    });
});

Template.singleArtist.helpers({
    artist() {
        return FlowRouter.current().path.replace("/", "").replace(/%20/gi, " ");
    },
    images() {
        const artist = FlowRouter.current().path.replace("/", "").replace(/%20/gi, " ");
        const recentImages = ImageIds.find({artist}, {limit: 15, sort: {date: -1}}).fetch();
        return recentImages.map((element) => {
          element.filePath = `/images/${normalizeStringToURLPath(element.artist)}/${element._id.toHexString()}.png`;
          element.artist = element.artist;
          element.shareData = {
            title: "LYRICSPAINTER",
            name: element.artist,
            description: "Wordclouds with your favorite artist's lyrics",
            image: element.filePath,
          };
          element.normalizedArtistName = normalizeString(element.artist);
          element.fileName = element.filePath.substr(element.filePath.lastIndexOf("/")+1,50);
          return element;
        })
    },
});

Template.singleArtist.events({
  'click #load-more'(event, instance) {
    event.target.blur();
    const newLimit = Session.get('limit') + 3;
    Session.set('limit', newLimit);
    const artist = FlowRouter.current().path.replace("/", "").replace(/%20/gi, " ");
    Meteor.subscribe('imageIdsFor', artist, newLimit);
    setTimeout(()=>{window.scrollBy(0,600 );}, 1000)
  },
  'load img'(event, instance) {
    //TODO image loaded
  }
});
