import './gallery.html';
import './../../components/gallery/gallery_template.html';
import _ from 'underscore'
import { ImageIds } from '/imports/api/genius/imageIds.js';
import { normalizeString, normalizeStringToURLPath } from '/imports/support/normalizeName';

Template.gallery.onCreated(function () {
  Session.set('limit', 9);
  this.autorun(function () {
    const limit = Session.get('limit');
    Meteor.subscribe('latestImages',   limit);
  });
});

Template.gallery.helpers({
    images() {
        const defaultImages = [];
        const galleryImages = ImageIds.find({}).fetch().map((imageDoc) => {
          return {
            filePath: `/images/${normalizeStringToURLPath(imageDoc.artist)}/${imageDoc._id.toHexString()}.png`,
            artist: imageDoc.artist,
          }
        });
        const imagesUnion = _.union(defaultImages, galleryImages);
        return imagesUnion.map((element)=>{
          element.shareData = {
            title: "LYRICSPAINTER",
            name: element.artist,
            description: "Wordclouds with your favorite artist's lyrics",
            image: element.filePath,
          };
          element.normalizedArtistName = normalizeString(element.artist);
          element.fileName = element.filePath.substr(element.filePath.lastIndexOf("/")+1,50);
          return element;
        })
    },
});

Template.gallery.events({
  'click #load-more'(event, instance) {
    const newLimit = Session.get('limit') + 3;
    event.target.blur();
    Session.set('limit', newLimit);
    Meteor.subscribe('latestImages', newLimit);
    setTimeout(()=>{window.scrollBy(0,600);}, 1000)
  },
  'load img'(event, instance) {
    //TODO image loaded
  }
});
