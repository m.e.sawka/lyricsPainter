import './monstersOfRock.html';
import './../../../components/gallery/gallery_template_2.html';
import { normalizeString } from '/imports/support/normalizeName';
Template.monstersOfRock.helpers({
  images() {
    const defaultImages = [
      {artist: "pink floyd",
        filePath:"https://lyricspainter.sawka.pro/images/pink_floyd/5910df9e8d940e317a9ef536.png"},
      {artist: "the beatles",
        filePath:"https://lyricspainter.sawka.pro/images/the_beatles/591448018d940e688852f008.png"},
      {artist: "doors",
      filePath:"https://lyricspainter.sawka.pro/images/the_doors/5910dae88d940e3029409b0e.png"},
      {artist: "led zeppelin",
        filePath:"https://lyricspainter.sawka.pro/images/led_zeppelin/5910dea38d940e313f525836.png"},
      {artist: "metallica",
        filePath:"https://lyricspainter.sawka.pro/images/metallica/5910e5468d940e32eb513476.png"},
      {artist: "sex pistols",
        filePath:"https://lyricspainter.sawka.pro/images/sex_pistols/5910e8c28d940e33c1717c59.png"},
      {artist: "radiohead",
        filePath:"https://lyricspainter.sawka.pro/images/radiohead/5910e3088d940e32570511ea.png"},
      {artist: "the police",
        filePath:"https://lyricspainter.sawka.pro/images/the_police/5910e2398d940e320aa984a0.png"},
      {artist: "red hot chili peppers",
        filePath:"https://lyricspainter.sawka.pro/images/red_hot_chili_peppers/5910e4728d940e32bd5150eb.png"},
      {artist: "the offspring",
        filePath:"https://lyricspainter.sawka.pro/images/the_offspring/5910db2f8d940e3037f234b0.png"},
      {artist: "korn",
        filePath:"https://lyricspainter.sawka.pro/images/korn/5910d7478d940e2f7ac06a4d.png"},
      {artist: "arctic monkeys",
        filePath:"https://lyricspainter.sawka.pro/images/arctic_monkeys/5910d8cb8d940e2fce0c5f3f.png"},
      {artist: "iron maiden",
        filePath:"https://lyricspainter.sawka.pro/examples/iron_maiden.png"},
      {artist: "judas priest",
        filePath:"https://lyricspainter.sawka.pro/images/judas_priest/5910e5d58d940e33017b7382.png"},
    ];

    return defaultImages.map((element) => {
      element.shareData = {
        title: "LYRICSPAINTER",
        name: element.artist,
        description: "Wordclouds with your favorite artist's lyrics",
        image: element.filePath,
      };
      element.normalizedArtistName = normalizeString(element.artist);
      element.fileName = element.filePath.substr(element.filePath.lastIndexOf("/") + 1, 50);
      return element;
    })
  },
});

