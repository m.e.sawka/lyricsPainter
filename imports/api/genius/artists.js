// Definition of the artists collection

import { Mongo } from 'meteor/mongo';

// Artists = new Mongo.Collection('artists');
export const Artists = new Mongo.Collection('artists');
export const ArtistsPending = new Mongo.Collection('artistsPending');
