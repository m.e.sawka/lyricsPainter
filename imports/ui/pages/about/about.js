import './about.html';
import { normalizeString } from '/imports/support/normalizeName';import {
  getArtistCallback,
  processNewArtist,
  getAllLyricsAndOnlySave,
  updateAllLyrics,
  makeImage,
  saveCanvasMask,
  dataURLtoBlob} from "../../../support/processNewArtist.js"


Template.about.events({
  'click button.toggle-info'(event, instance) {
    const element =  document.getElementsByClassName('more-info')[0];
    if (element.style.maxHeight != '1000px') {
      element.style.opacity = 1;
      element.style.padding = '30px 30px 30px 30px';
      element.style.maxHeight = '1000px';
    }else{
      element.style.opacity = 0;
      element.style.maxHeight = '0px';
      element.style.padding = '0';

    }
    event.target.blur()
  },
  'submit #aboutArtist'(event, instance) {
    event.preventDefault();
    let artistName = document.querySelector('#artistInput').value;
    Session.set('userOpts', {
      wordColor: { h: 58, s: 100, l: 50, hex: '#fff700'},
      backgroundColor: { h: 258, s: 100, l: 40, hex: '#3e00ce'},
      maskFileName: null,
      tmpMaskFilePath: null,
      artistNameFont: "Roboto",
      wordcloudFont: "Roboto",
    })
    FlowRouter.go('/create');
    Meteor.call('getArtist', artistName, getArtistCallback);
  }
});

Template.about.helpers({
  sampleImages() {
    const sampleArtists = ['Michael Jackson', 'Iron Maiden', 'Wu-Tang Clan'];
    return sampleArtists.map((artistName) => {
      return {
        filePath: `/examples/${artistName.toLowerCase().replace(/ /g, '_')}.png`,
        artistName,
        normalizedArtistName: normalizeString(artistName)
      }
    })
  }
})

