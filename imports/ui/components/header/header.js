import './header.html';

Template.headerNav.onCreated(function headerNavOnCreated() {

});



Template.headerNav.events({
    'click *'(event) {
        const hiddenMenu = document.querySelector(".navbar-collapse.collapse.in");
        if (hiddenMenu){
            let elementToCheck = event.target;
            while (elementToCheck) {
                if (elementToCheck.classList.contains('navbar-toggle')) {
                    return
                };
                elementToCheck = elementToCheck.parentElement;
            }
            document.querySelector(".navbar-collapse.collapse.in").classList.remove('in')
        }
    },
    'click a[href="/create"]'(event) {
      FlowRouter.reload('/create');
    }
});
