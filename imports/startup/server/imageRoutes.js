var fs = require('fs');

import {Picker} from 'meteor/meteorhacks:picker';
Picker.route('/images/:artistName/:imageId', function (params, request, response, next) {
    const artistName = params.artistName;
    const imageId = params.imageId;
    var filePath = `${process.env.PWD}/.images/${artistName}/${imageId}`;
    try {
      var data = fs.readFileSync(filePath);
      response.setHeader('Content-Type', 'image');
      response.statusCode = 200;
      response.end(data);
    } catch (err) {
      if (err.code === 'ENOENT') {
        console.log('File not found! imageId:', params.imageId);
      } else {
        console.log('batman: error', error);
        throw err;
      }
    }
});

Picker.route('/examples/:artistName', function (params, request, response, next) {
    const artistName = params.artistName;
    var filePath = `${process.env.PRIVATE_PATH}/examples/${artistName}`;
    var data = fs.readFileSync(filePath);
    response.setHeader('Content-Type', 'image');
    response.statusCode = 200;
    response.end(data);
});