#TO RUN export PYTHONIOENCODING=utf-and (maybe) nltk.download('punk')
#!/usr/bin/python
# -*- coding: <UTF-8> -*-
import re
from pymongo.collection import ReturnDocument
import pymongo
import sys
from nltk import word_tokenize, FreqDist
from bson import objectid
from wordcloud import WordCloud, STOPWORDS, ImageColorGenerator

#local
#client = pymongo.MongoClient(host='127.0.0.1', port=3001)
#db = client['meteor']


##server
client = pymongo.MongoClient()
db = client['lyricspainter']

songsCollection = db.songs
artistsCollection = db.artists

print(songsCollection)
print(artistsCollection)

def stopwords_list():
    stopwords = [';', '<', '/b', '>', "''", '``', '--', '(', ')', '[', ']', '-', ':', '–', ',', '.', '?', '!', '...', 'outro', 'cuty', 'hook', 'guitar', 'solo', 'chorus', 'verse', 'Guitar', 'Solo', 'Chorus', 'Verse', 'Intro', 'Outro', 'x2', 'ref', 'x3', '3x', '2x', 'zwrotka', 'refren', 'x2', 'x4', '4x', 'Ref', '2x', 'a', 'aby', 'ach', 'acz', 'aczkolwiek', 'aj', 'albo', 'ale', 'ależ', 'ani', 'aż', 'bardziej', 'bardzo', 'bo', 'bowiem', 'by', 'byli', 'bynajmniej', 'być', 'był', 'była', 'było', 'były', 'będzie', 'będą', 'cali', 'cała', 'cały', 'ci', 'cię', 'ciebie', 'co', 'cokolwiek', 'coś', 'czasami', 'czasem', 'czemu', 'czy', 'czyli', 'daleko', 'dla', 'dlaczego', 'dlatego', 'do', 'dobrze', 'dokąd', 'dość', 'dużo', 'dwa', 'dwaj', 'dwie', 'dwoje', 'dziś', 'dzisiaj', 'gdy', 'gdyby', 'gdyż', 'gdzie', 'gdziekolwiek', 'gdzieś', 'i', 'ich', 'ile', 'im', 'inna', 'inne', 'inny', 'innych', 'iż', 'ja', 'ją', 'jak', 'jakaś', 'jakby', 'jaki', 'jakichś', 'jakie', 'jakiś', 'jakiż', 'jakkolwiek', 'jako', 'jakoś', 'je', 'jeden', 'jedna', 'jedno', 'jednak', 'jednakże', 'jego', 'jej', 'jemu', 'jest', 'jestem', 'jeszcze', 'jeśli', 'jeżeli', 'już', 'ją', 'każdy', 'kiedy', 'kilka', 'kimś', 'kto', 'ktokolwiek', 'ktoś', 'która', 'które', 'którego', 'której', 'który', 'których', 'którym', 'którzy', 'ku', 'lat', 'lecz', 'lub', 'ma', 'mają', 'mało', 'mam', 'mi', 'mimo', 'między', 'mną', 'mnie', 'mogą', 'moi', 'moim', 'moja', 'moje', 'może', 'możliwe', 'można', 'mój', 'mu', 'musi', 'my', 'na', 'nad', 'nam', 'nami', 'nas', 'nasi', 'nasz', 'nasza', 'nasze', 'naszego', 'naszych', 'natomiast', 'natychmiast', 'nawet', 'nią', 'nic', 'nich', 'nie', 'niech', 'niego', 'niej', 'niemu', 'nigdy', 'nim', 'nimi', 'niż', 'no', 'o', 'obok', 'od', 'około', 'on', 'ona', 'one', 'oni', 'ono', 'oraz', 'oto', 'owszem', 'pan', 'pana', 'pani', 'po', 'pod', 'podczas', 'pomimo', 'ponad', 'ponieważ', 'powinien', 'powinna', 'powinni', 'powinno', 'poza', 'prawie', 'przecież', 'przed', 'przede', 'przedtem', 'przez', 'przy', 'roku', 'również', 'sama', 'są', 'się', 'skąd', 'sobie', 'sobą', 'sposób', 'swoje', 'ta', 'tak', 'taka', 'taki', 'takie', 'także', 'tam', 'te', 'tego', 'tej', 'temu', 'ten', 'teraz', 'też', 'to', 'tobą', 'tobie', 'toteż', 'trzeba', 'tu', 'tutaj', 'twoi', 'twoim', 'twoja', 'twoje', 'twym', 'twój', 'ty', 'tych', 'tylko', 'tym', 'u', 'w', 'wam', 'wami', 'was', 'wasz', 'wasza', 'wasze', 'we', 'według', 'wiele', 'wielu', 'więc', 'więcej', 'wszyscy', 'wszystkich', 'wszystkie', 'wszystkim', 'wszystko', 'wtedy', 'wy', 'właśnie', 'z', 'za', 'zapewne', 'zawsze', 'ze', 'zł', 'znowu', 'znów', 'został', 'żaden', 'żadna', 'żadne', 'żadnych', 'że', 'żeby']
    stopwords_en = ["its", "ill", "dont", "im", "a", "able", "about", "across", "after", "all", "almost", "also", "am", "among", "an", "and", "any", "are", "as", "at", "be", "because", "been", "but", "by", "can", "cannot", "could", "dear", "did", "do", "does", "either", "else", "ever", "every", "for", "from", "get", "got", "had", "has", "have", "he", "her", "hers", "him", "his", "how", "however", "i", "i'm", "it's", "i'll", "don't", "if", "in", "into", "is", "it", "its", "just", "least", "let", "like", "likely", "may", "me", "might", "most", "must", "my", "neither", "no", "nor", "not", "of", "off", "often", "on", "only", "or", "other", "our", "own", "rather", "said", "say", "says", "she", "should", "since", "so", "some", "than", "that", "the", "their", "them", "then", "there", "these", "they", "this", "tis", "to", "too", "twas", "us", "wants", "was", "we", "were", "what", "when", "where", "which", "while", "who", "whom", "why", "will", "with", "would", "yet", "you", "your"]
    stopwords += stopwords_en
    stopwords_cap = []
    print(len(stopwords))
    for word in stopwords:
        stopwords_cap.append(word.capitalize())
    stopwords += stopwords_cap
    print(len(stopwords))
    return stopwords
stopwords = stopwords_list()


def update_songs_stats(songList):
    artist_lyrics_all = ''
    for song in songList:
        try:
            print(song['title'])
            #if 'lyrics' in song.keys() and isinstance(song['lyrics'], str) and songs.matchedWithAlbum == 1:
            if 'lyrics' in song.keys() and isinstance(song['lyrics'], str):
                lyrics = song['lyrics']
                lyrics_clean = lyrics.lower()
                lyrics_clean = re.sub('\[.*?\]', '', lyrics_clean)
                print(len(lyrics))
                # Find a better way to prevent splitting words with apostrophe into two tokens
                lyrics_clean = re.sub("'", '', lyrics_clean)
                lyrics_tokens = word_tokenize(lyrics_clean)
                artist_lyrics_all += lyrics_clean
                wordcount = len(lyrics_tokens)
                unique = len(set(lyrics_tokens))

                filtered_lyrics = [word for word in lyrics_tokens if word not in stopwords]
                song_stopwords = [word for word in lyrics_tokens if word in stopwords]
                stopwords_count = len(song_stopwords)
                wordcount_filtered = len(filtered_lyrics)
                unique_filtered = len(set(filtered_lyrics   ))

                song['wordcount'] = wordcount
                song['unique'] = unique
                song['stopwords_count'] = stopwords_count
                song['wordcount_filtered'] = wordcount_filtered
                song['unique_filtered'] = unique_filtered

                fields_to_update = {
                    'stats': {
                        'wordcount': wordcount,
                        'unique': unique,
                        'stopwords_count': stopwords_count,
                        'wordcount_filtered': wordcount_filtered,
                        'unique_filtered': unique_filtered
                    }
                }
            else:
                print('IN ELSE'+ str(song['title']))
                fields_to_update = {
                    'stats': {}
                }

            returned_document = songsCollection.find_one_and_update(
                {'_id': song['_id']},
                {'$set': fields_to_update},
                upsert=True,
                return_document=pymongo.ReturnDocument.AFTER
            )

            # print(returned_document)
            # print("================================")

        except:
            print("ERROR MATE")
            print("Unexpected error:", sys.exc_info()[0])
            pass

    return artist_lyrics_all


def update_stats_for_artist(artistId):
    songlist = list(songsCollection.find({"primary_artist.id": artistId}))
    all_artist_lyrics = update_songs_stats(songlist)
    updated_songlist = list(songsCollection.find({"primary_artist.id": artistId}))

    ###TODO make it better
    wordcount_list = []
    for song in updated_songlist:
        #print (wordcount_list)
        #print(song['stats'])
        if 'stats' in song.keys() and 'wordcount' in song['stats'].keys():
            wordcount_list.append(song['stats']['wordcount'])
    ###

    if len(wordcount_list)>0:
        max_words = max(wordcount_list)
        song_with_max_words = list(filter(lambda song: song['stats'] and song['stats']['wordcount'] and song['stats']['wordcount'] == max_words, updated_songlist))[0]
        artist_tokens = word_tokenize(all_artist_lyrics)
        all_words_length = len(''.join(artist_tokens))
        avg_word_length = all_words_length / len(artist_tokens)
        wordcount = len(artist_tokens)
        unique = len(set(artist_tokens))

        filtered_words = [word for word in artist_tokens if word not in stopwords]
        all_filtered_words_length = len(''.join(filtered_words))
        avg_filtered_word_length = all_filtered_words_length / len(filtered_words)
        fdist_filtered = FreqDist(filtered_words)
        filtered_wordcount = len(filtered_words)
        filtered_unique = len(set(filtered_words))
        total_songs = len(updated_songlist)
        most_common = fdist_filtered.most_common(25)
        longest_song = {
                'title': song_with_max_words['title'],
                'path': song_with_max_words['path'],
                '_id': song_with_max_words['_id'],
                'words': max_words
            }

        stats = {
            'longest_song': longest_song,
            'total_songs': total_songs,
            'wordcount': wordcount,
            'unique': unique,
            'filtered_wordcount': filtered_wordcount,
            'filtered_unique': filtered_unique,
            'most_common': most_common,
            'avg_word_length': round(avg_word_length,2),
            'avg_filtered_word_length': round(avg_filtered_word_length,2)
        }
        updateObject = {
            'stats': stats
        }

        returned_document = artistsCollection.find_one_and_update(
                {'artistId': artistId},
                {'$set': updateObject},
                upsert=True,
                return_document=pymongo.ReturnDocument.AFTER
            )

        print(returned_document)
    else:
        print('no lyrics for'+ str(artistId))
    print("================================")


def updateAllStats():
    artistsList = list(artistsCollection.find({}))
    for artist in artistsList:
        try:
            if artist['artistName']:
                print(artist['artistName'])
                update_stats_for_artist(artist['artistId'])
        except:
            print("Unexpected error:", sys.exc_info()[0])
            pass

#updateAllStats()
#update_stats_for_artist(58540) #ostr
#update_stats_for_artist(22163) #iron maiden
#update_stats_for_artist(1174) #kings of leon
update_stats_for_artist(53880) #zbuku