import './kingsOfRap.html';
import './../../../components/gallery/gallery_template_2.html';
import { normalizeString } from '/imports/support/normalizeName';
Template.kingsOfRap.helpers({
  images() {
    const defaultImages = [{
      artist: 'the notorious b.i.g.',
      filePath: 'https://lyricspainter.sawka.pro/images/the_notorious_big/59026e108d940e4e95cfaa58.png',
    }, {
        filePath: `https://lyricspainter.sawka.pro/images/jay_z/58e2bad58d940e5e0e3d1e69.png`,
        artist: 'Jay Z'
      }, {
        filePath: `https://lyricspainter.sawka.pro/images/nas/58eac3a48d940e5eea237013.png`,
        artist: 'Nas'
      },
      {
        artist: 'snoop dogg',
        filePath: 'https://lyricspainter.sawka.pro/images/snoop_dogg/590257c38d940e4a06d732c0.png',
      },
      {
        artist: 'krs-one',
        filePath: 'https://lyricspainter.sawka.pro/images/krs-one/590248218d940e46e6d664cd.png',
      },
      {
        artist: 'ice cube',
        filePath: 'https://lyricspainter.sawka.pro/images/ice_cube/590253ad8d940e48ca593c83.png',
      },
      {
        artist: 'eminem',
        filePath: 'https://lyricspainter.sawka.pro/images/eminem/59027c598d940e51af8b1412.png',
      },
      {
        artist: 'lauryn hill',
        filePath: 'https://lyricspainter.sawka.pro/images/lauryn_hill/5902401d8d940e44348bd989.png',
      },{
        artist: 'method man',
        filePath: 'https://lyricspainter.sawka.pro/images/method_man/59025fde8d940e4ba4f5af9e.png',
      },
      {
        filePath: `https://lyricspainter.sawka.pro/images/dr_dre/58e33f978d940e76f4b72692.png`,
        artist: 'Dr Dre'
      },
    ];

    return defaultImages.map((element) => {
      element.shareData = {
        title: "LYRICSPAINTER",
        name: element.artist,
        description: "Wordclouds with your favorite artist's lyrics",
        image: element.filePath,
      };
      element.normalizedArtistName = normalizeString(element.artist);
      element.fileName = element.filePath.substr(element.filePath.lastIndexOf("/") + 1, 50);
      return element;
    })
  },
});

