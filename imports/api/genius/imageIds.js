// Definition of the images collection

import { Mongo } from 'meteor/mongo';

export const ImageIds = new Mongo.Collection('imageIds');
