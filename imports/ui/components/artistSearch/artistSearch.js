import {Artists} from '/imports/api/genius/artists.js';
import {ImageIds} from '/imports/api/genius/imageIds.js';
import {Meteor} from 'meteor/meteor';
import './artistSearch.html';
import './imageOptions.js';
import './leaveEmail.js';
import '/imports/ui/components/galleryRow/galleryRow.html';
import { normalizeString, normalizeStringToURLPath } from '/imports/support/normalizeName';
import { getArtistCallback, saveCanvasMask } from "../../../support/processNewArtist.js"

Template.artistSearch.helpers({
    artists() {
        return Artists.find({});
    },
    status() {
        let status = Session.get('imageGenStatus');
        if (Array.isArray(status)) {
            status = status.map((x) => {
                return x
            })
        } else if (status) {
            status = [status]
        }
        return status;
    },
    artist() {
      const currentArtist = Session.get('currentArtist');
      if (currentArtist) {
        Meteor.subscribe('imageIdsFor', currentArtist.artistName, 3);
        return currentArtist
      }
    },
    generatedImage() {
        const _id = Session.get('generatedImageId');
        if (_id) {
          const artistNamePath = normalizeString(Session.get('currentArtist').artistName).replace(/ /g, '_');
          return `/images/${artistNamePath}/${_id}.png`;
        }
    },
    shareData() {
      const name = Session.get('currentArtist') ? normalizeString(Session.get('currentArtist').artistName) : "";
      const _id = Session.get('generatedImageId');
      return {
        title: "LYRICSPAINTER - create wordclouds with your favorite artist's lyrics",
        name,
        image: `https://lyricspainter.sawka.pro/images/${name}/${_id}`,
      }
    },
    showStatus() {
      return Session.get('imageGenStatus') || window.document.getElementById("generatedImageWrapper") && !window.document.getElementById("generatedImageWrapper").style.display == 'block';
    },
    otherImages() {
        let images = [];
        const artistName = Session.get('currentArtist') ? Session.get('currentArtist').artistName : null;
        let mappedImages;
        if (artistName) {
            const images = ImageIds.find({"artist": artistName}, {limit: 3, skip: 1, sort: {date: -1}}).fetch();
            mappedImages = images.map((element) => {
            element.filePath = `/images/${normalizeStringToURLPath(artistName)}/${element._id.toHexString()}.png`;
            element.shareData = {
              title: "LYRICSPAINTER",
              name: element.artist,
              description: "Wordclouds with your favorite artist's lyrics",
              image: element.filePath,
            };
            element.normalizedArtistName = normalizeString(element.artist);
            element.fileName = element.filePath.substr(element.filePath.lastIndexOf("/")+1,50);
            return element;
          })
        }
        return mappedImages
    },
    generatedImageBorder() {
      return Session.get('userOpts').backgroundColor.hex
    },
    leaveEmail() {
      return Session.get('leaveEmail');
    }
});

Template.artistSearch.events({
    'click .submitArtist'(event) {
        event.preventDefault();
        const artistInput = document.querySelector('#artistInput');
        let artistName = artistInput.value;
        if (!artistName) {
          window.scrollTo(0, artistInput.offsetTop);
          artistInput.className += ' marked';
          artistInput.setAttribute('placeholder', 'Please provide artist name!')
        } else {
          if (Session.get('canvasImage') === true) {
            saveCanvasMask()
          }
          console.log('IMAGE GENERATION START', new Date());
          Meteor.call('getArtist', artistName, getArtistCallback)
        }
  },'load img.generatedImage'(event, instance) {
    if (window.document.getElementsByClassName("generatedImage")[0].height) {
      window.document.getElementById("generatedImageWrapper").style.display = "block"
    }
  },
});
