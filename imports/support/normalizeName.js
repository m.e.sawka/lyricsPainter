export function normalizeString (string) {
    return string.replace(/[^\x00-\x7F]/g, "_").replace(/\.|\//gi, '').replace(/^\s+|\s+$/gm, '').toLowerCase();
}

export function normalizeStringToURLPath (string) {
    return normalizeString(string).replace(/ /g, "_");
}